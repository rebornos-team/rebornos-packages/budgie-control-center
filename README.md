# budgie-control-center

Budgie's main interface to configure various aspects of the desktop

https://github.com/BuddiesOfBudgie/budgie-control-center

<br>

How to clone this repository:
```
git clone https://gitlab.com/rebornos-team/rebornos-packages/budgie/budgie-control-center.git
```
